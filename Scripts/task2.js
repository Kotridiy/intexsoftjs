function task2() {
    var arr = {}
    console.log("Experement #1:" + (String(undefined))); //boring
    //console.log("Experement #2:" + (var ell + 'smth')); //don't work
    console.log("Experement #3:" + ('you' + String(['a','r','e']) + {num: "nothing"})); //strange
    console.log("Experement #4:" + (56 + +String('7' + '5' + 11))); //7567
    console.log("Experement #5:" + (parseInt('try4me'))); // boring
    console.log("Experement #6:" + ((-1/0)/(-1/0))); //oooow
    console.log("Experement #7:" + (null - true)); //boring
    console.log("Experement #8:" + (+(+'\n'==false))); //boring
    console.log("Experement #9:" + (undefined == undefined)); //true?
    console.log("Experement #10:" + (undefined === undefined)); //true
    console.log("Experement #11:" + ({val:'my ego'} == ['bad words'])); //false????
    console.log("Experement #12:" + (null == undefined)); //true of course
    console.log("Experement #13:" + (!null == !undefined)); //true too
    console.log("Experement #14:" + (!!NaN)); //false
    console.log("Experement #15:" + ("false" == false)); //false
    console.log("Experement #16:" + (5/'no please' === 5/'no please')); //as I was expecting smth else...
    console.log("Experement #FINAL:" + ( ~~+Date.now() % 6 + 660 >= (15 | 16)*20 && "MONEY!" ?
        'Good ' + ((new Date).getHours() > 16 ? 'morning' : 'evening') : 666..toString()));
}